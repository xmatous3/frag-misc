package Frag::Misc::SleepyDaemon;
# Zbastlil adamat <xmatous3@fi.muni.cz>, 2020

use warnings;
use strict;
use v5.20;
use utf8;

use Data::Dumper;
use File::Slurp qw/read_file write_file/;
use DBI;

use Exporter 'import';
our @EXPORT_OK = qw/run/;

sub run($) {
    my $cfg = shift;

    $cfg->{from} = uc ($cfg->{subject}) . " Mailer <$cfg->{subject}\@fi.muni.cz>"
        unless $cfg->{from};
    say Dumper $cfg if -t STDOUT;

    my $workdir = $cfg->{workdir};
    my $stampfile = "$workdir/.stamp";
    my $pidfile = "$workdir/.pid";
    my $wakefile = "$workdir/wake";

    `mkdir -p '$cfg->{workdir}'`;

    unless ( -f $stampfile ) {
        say "Creating a new stamp file." if -t STDOUT;
        write_file( $stampfile, $cfg->{stamp} // scalar time );
        exit 0 unless $cfg->{loop};
    }

    my $dbh = DBI->connect( "dbi:Pg:dbname=$cfg->{subject};host=$cfg->{dbhost}" )
        or die;

    my $stopped = 0;
    my $stop_handler = sub { $stopped = 1; };
    $SIG{INT} = $stop_handler;
    $SIG{TERM} = $stop_handler;

    if ( $cfg->{loop} ) {
        $SIG{USR1} = sub {};
        write_file( $pidfile, $$ );
        write_file( $wakefile, <<~EOF ) unless -f $wakefile;
            \#!/bin/bash
            pidf='$pidfile'
            pkill -USR1 -F \$pidf || ( rm \$pidf; echo "No daemon to wake up!"; false )
            EOF
        chmod 0744, $wakefile;
    }

    sleep if $cfg->{wait};
    RUN: until ( $stopped ) {
        my $stamp = $cfg->{stamp} // read_file( $stampfile );
        my $newstamp = time;

        print `date` if -t STDOUT;

        $dbh->ping()
            or die "$workdir: Database connexion lost.";

        my $ret = $cfg->{job}->( $dbh, $stamp, $cfg );

        if ( defined $ret && ! $ret ) {
            say STDERR "$workdir: Job failed.";
            say STDERR "(stamp not updated to $newstamp)";
        } else {
            write_file( $stampfile, $newstamp ) unless $cfg->{'dry-run'};
        }

        last RUN unless $cfg->{loop};
        say '=' x 60;
        sleep;  # wait for USR1
    }

    $dbh->disconnect();
    say 'Exiting' if $cfg->{loop};
}

1;
