#!/usr/bin/perl
# Zbastlil adamat <xmatous3@fi.muni.cz>, 2021

use strict;
use warnings;
use utf8;
use v5.10;

use DBI;
use MIME::Lite;
use Encode qw/encode_utf8 decode_utf8 encode/;
use File::Slurp qw/read_file/;
use Data::Dumper;

my $cfg = {
    assignment => 'big2',   # name of the frag assignment
    course => lc $ENV{FRAG_SUBJECT},
};

my $force = @ARGV && $ARGV[0] =~ /^(-f|--force)$/;

my $connstr = "dbi:Pg:dbname=$ENV{FRAG_SUBJECT};options='--search_path=frag,public'";
$connstr .= ";host=$ENV{FRAG_HOST}" if defined $ENV{FRAG_HOST};
$connstr .= ";user=$ENV{FRAG_USER}" if defined $ENV{FRAG_USER};
my $dbh = DBI->connect( $connstr ) or die;

$dbh->begin_work;


my $sth = $dbh->prepare( <<'EOF' ) or die;
    select re.login reviewer, au.login author, au.id auid, c.data pad
    from review r
    join review_request rr on rr.id = r.request_id and peer
    join eval_latest el on el.submission_id = r.submission_id
    join eval_out eo on eo.eval_id = el.id
    join submission s on s.id = r.submission_id
    join pad_assignment pa on pa.student = s.author
    join assignment a on a.id = pa.assignment_id
                     and a.id = s.assignment_id
    join content c on c.sha = pa.content_sha
    join person au on au.id = pa.student
    join person re on re.id = r.reviewer
    where a.name = ?
      and eo.name = 'group'
      and eo.group <> 'meta'
      and not passed
EOF

$sth->execute( $cfg->{assignment} ) or die;
while ( my $r = $sth->fetchrow_hashref ) {

    my $subj = sprintf '[%s] Výstup testů recensovaného řešení (%s, %s)',
                        uc $cfg->{course}, $r->{author}, $cfg->{assignment};
    my $pad = decode_utf8( $r->{pad} );
    $pad =~ s/^testing .* of submission #\d+ from .*$//m;
    my $body = <<EOF;
Hezký den,

vámi recensovanému řešení úlohy $cfg->{assignment} studenta $r->{author}, učo $r->{auid}
neprošly všechny testy. Jejich výstup je součástí tohoto automatického dopisu.

S pozdravem
Rozesílač výstupu testů recensentům
$cfg->{course}\@fi


Následuje výstup testů.

--------- 8< ---------
$pad
EOF
    my $msg = MIME::Lite->new(
        From    => $cfg->{from} // "$cfg->{course}\@fi.muni.cz",
        To      => "$r->{reviewer}\@fi.muni.cz",
        # Cc      => 'matousek@fi.muni.cz',
        Subject => encode( 'MIME-Header', $subj ),
        Data    => encode_utf8( $body )
    );
    $msg->attr( 'content-type' => 'text/plain; charset=utf-8');
    $msg->print;
    $msg->send( 'sendmail', '/usr/lib/sendmail -t -X ""') if $force;
}

say "\nNothing was sent, use -f or --force." unless $force;

$dbh->rollback;
