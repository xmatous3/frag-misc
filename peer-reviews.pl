#!/usr/bin/perl
# Zbastlil adamat <xmatous3@fi.muni.cz>, 2021

use strict;
use warnings;
use utf8;

use v5.10;

use List::Util qw/shuffle/;
use DBI;

my $cfg = {
    assignment => 'big2',   # name of the frag assignment
    peers => 2,             # reviews per student (both created and received)
};

my $force = @ARGV && $ARGV[0] =~ /^(-f|--force)$/;
my $interactive = @ARGV && $ARGV[0] =~ /^(-i|--interactive)$/;

my $connstr = "dbi:Pg:dbname=$ENV{FRAG_SUBJECT}";
$connstr .= ";host=$ENV{FRAG_HOST}" if defined $ENV{FRAG_HOST};
$connstr .= ";user=$ENV{FRAG_USER}" if defined $ENV{FRAG_USER};
my $dbh = DBI->connect( $connstr ) or die;

$dbh->begin_work;
my $sth = $dbh->prepare( <<EOF ) or die;
    select p.login login, p.id uid, sl.id sid
    from frag.submission_latest sl
    join frag.assignment a on sl.assignment_id = a.id
    join frag.person p on sl.author = p.id
    where a.name = ?
      and p.id not in (select teacher from frag.tutor)
EOF

$sth->execute( $cfg->{assignment} ) or die;
my @p;
while ( my $r = $sth->fetchrow_hashref ) {
    push @p, $r;
}

@p > $cfg->{peers} or die 'nothing to do';

$sth = $dbh->prepare( <<EOF ) or die;
    select distinct reviewer, author
    from frag.review r
    join frag.submission s on s.id = r.submission_id
    where r.reviewer not in (table frag.teacher_list)
EOF
my %past;
$sth->execute() or die;
while ( my $r = $sth->fetchrow_hashref ) {
    $past{ "$r->{reviewer};$r->{author}" } = 1;
}

my %hither;
my %yon;
my %offsets;

$offsets{ 1 + int( rand $#p ) } = 1 while keys %offsets < $cfg->{peers};

for my $i ( 0 .. $#p ) {
    for (keys %offsets) {
        my $n = ($i + $_) % @p;
        push $hither{$i}->@*, $n;
        push $yon{$n}->@*, $i;
    }
}

my @people = shuffle @p;

while ( my ($who, $whom) = each %hither ) {
    say "$people[$who]->{login}  reviews  " . join ', ', map $people[$_]->{login}, @$whom;
}

say '';

while ( my ($whom, $who) = each %yon ) {
    say "$people[$whom]->{login}  reviewed by  " . join ', ', map $people[$_]->{login}, @$who;
}

say '';

while ( my ($who, $whom) = each %hither ) {
    my $reviewer = $people[$who];
    for ( @$whom ) {
        my $reviewee = $people[$_];
        $reviewer->{uid} != $reviewee->{uid} or die;
        if ( $past{"$reviewer->{uid};$reviewee->{uid}"} ) {
            say "[!] $reviewer->{login} has already reviewed $reviewee->{login}";
        }
        my ($req_id) = $dbh->selectrow_array(
                'insert into frag.review_request (submission_id, peer) values (?, true) returning id',
                {}, $reviewee->{sid} );
        $dbh->do( 'insert into frag.review (request_id, reviewer, submission_id) values (?, ?, ?)',
                  {}, $req_id, $reviewer->{uid}, $reviewee->{sid} );
    }
}

if ( $interactive ) {
    say "\n", "-" x 20;
    say "Accept this solution?";
    my $ans = <STDIN>;
    if ( $ans =~ /^(yes|y)$/ ) {
        $force = 1;
    }
}

if ( $force ) {
    $dbh->commit or die;
    say "\nCHANGED IN THE DB!";
} else {
    $dbh->rollback;
    say "\nThis is a dry run; use -f or --force to make changes in the DB";
}
