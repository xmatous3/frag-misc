#!/usr/bin/perl
# Zbastlil adamat <xmatous3@fi.muni.cz>, 2020

use warnings;
use strict;
use v5.20;
use utf8;

use MIME::Lite;
use Getopt::Long qw/GetOptions/;
use Encode qw/encode_utf8/;
use File::Slurp qw/read_file/;

use lib '.';
use Frag::Misc::SleepyDaemon 'run';

binmode STDOUT, ':utf8';
binmode STDERR, ':utf8';

my %options;
GetOptions( \%options,
    'loop|l',
    'dry-run|n',
    'stamp=i',
    'wait',
) or die;

# CONFIGURATION
# Put mail recipients' logins (or addresses) into ‹workdir›/mailto, one per line
# Spams ‹login›@fi.muni.cz unless the mailto file contains a recipient.

%options = ( %options,
        subject => 'pv264',
        dbhost  => 'frag-db.fi.muni.cz',
        workdir => '/home/xmatous3/pv264/mailer/submission-announcer',
        job     => \&check_and_mail,
    );


run( \%options );

exit 0;

##############################################################################

sub check_and_mail {
    my ($dbh, $stamp, $options) = @_;

    my $should_mail = 0;

    my $q_newest = <<~'EOF';
    passed as
        (select eo.eval_id from frag.eval_out as eo
        where eo.group = 'verity'
        group by eval_id
        having every(eo.passed)),
     newest as
        (select distinct on (s.author, s.assignment_id)
            s.author, s.assignment_id, s.id as sid, s.stamp
            from passed join frag.eval as e on passed.eval_id = e.id
            join frag.submission as s on submission_id = s.id
            order by s.author, s.assignment_id, s.stamp desc)
    EOF

    my $q_claims = <<~'EOF';
    teachers as
       (select teacher from frag.teacher_list),
    claims as
       (select distinct login as assignee, author, assignment_id as aid
           from frag.review
           join frag.submission as s on s.id = submission_id
           join frag.person on person.id = reviewer
           where reviewer in (table teachers))
    EOF

    my $sth = $dbh->prepare( <<~EOF );
    with $q_newest,
         $q_claims,
         recent as
            (select *
                from newest
                where newest.author not in (table teachers)
                    and newest.stamp > to_timestamp( ? )
                ),
         subjoin as
            (select recent.*, recent.assignment_id as aid, recent.author as uid, assignee
                from recent
                left outer join claims as c
                    on (c.author, c.aid) = (recent.author, recent.assignment_id))
    select p.login as login, p.name,
        j.sid as sub_id, a.name as hw, j.stamp as submitted, assignee
        from subjoin as j
        join frag.assignment as a on a.id = j.aid
        join frag.person as p on p.id = j.uid
        order by j.stamp asc, assignee, a.name
    EOF

    $sth->execute( $stamp )
        or die;

    my @new_subs;

    while ( my $r = $sth->fetchrow_hashref ) {
        my $assignee = $r->{assignee} ? "→ $r->{assignee}": 'AVAILABLE';
        my $when = $r->{submitted};
        $when =~ s/\d\d\d\d-(\d\d)-(\d\d) (\d\d:\d\d).*/$2. $1. $3/;
        my $s = "    • $r->{sub_id} by $r->{name} ($r->{login}) – $r->{hw} on $when ($assignee)";
        push @new_subs, $s;
        $should_mail = 1;
    }

    $sth = $dbh->prepare( <<~EOF );
    with $q_newest,
         $q_claims,
         review_done as
             (select distinct review_id as rid, submission_id as sid
                 from frag.annotation)

     select login, pa.id as uco, n.sid as sub_id, a.name as hw, assignee,
            extract(day from now() - n.stamp) as days
         from newest as n
         join frag.person as pa on pa.id = n.author
         join frag.assignment as a on a.id = n.assignment_id
         left outer join claims as c on (c.author, c.aid) = (n.author, n.assignment_id)
         where n.sid not in (select sid from review_done)
           and n.author not in (table teachers)
         order by n.stamp asc
    EOF

    $sth->execute()
        or die;

    my @waiting;

    while ( my $r = $sth->fetchrow_hashref ) {
        $should_mail = 1;
        my $assignee = $r->{assignee} ? "→ $r->{assignee}": 'AVAILABLE';
        my $tag = "";
        if ( $r->{days} > 3 ) {
            $should_mail = 1;
            $tag = $r->{days} > 5 ? " [URGENT!]" : " [soon]"
        }
        my $s = "    • $r->{days}d old: $r->{login}'s ($r->{uco}) $r->{hw} (ID $r->{sub_id}, $assignee)$tag";
        push @waiting, $s;
    }

    $sth = $dbh->prepare( <<~EOF );
    with $q_claims
    select assignee, a.name as hw, array_agg(  convert_from(pa.login, 'UTF8') ) as reviewees
        from claims as c
        join frag.assignment as a on a.id = c.aid
        join frag.person as pa on pa.id = c.author
        group by assignee, hw
        order by assignee, hw
    EOF

    $sth->execute()
        or die;

    my @mapping;

    while ( my $r = $sth->fetchrow_hashref ) {
        my @rees = sort @{ $r->{reviewees} };
        $" = ', ';
        my $s = "    • $r->{assignee} has " . @rees . "× $r->{hw}:";
        my $first = 1;
        while ( @rees ) {
            $s .= ',' unless $first;
            $s .= "\n          @{[grep {defined} @rees[0..4]]}";
            splice @rees, 0, 5;
            $first = 0;
        }
        push @mapping, $s;
    }

    $" = ",\n";

    my $Subject = uc $options->{subject};

    my $body = "Hello,\n\n";

    $body .= <<~EOF if @new_subs;
    There are new passing $Subject submissions:

    @new_subs.

    EOF

    my $_Subject = @new_subs ? '' : ' ' . $Subject;
    $body .= <<~EOF if @waiting;
    These$_Subject submissions are currently waiting to be reviewed:

    @waiting.

    EOF

    $" = ";\n";
    $body .= <<~EOF if @mapping;
    The hitherto claimed submissions are reviewed by:

    @mapping.

    EOF

    $body .= <<~EOF;
    Handy commands to start reviewing:

        \$ ‹frag› review --checkout --submission-id ‹ID›
        \$ ‹frag› review --checkout --redo                 # resubmits for you

    Regards
    -- 
    $Subject Mailer
    EOF

    say $body if -t STDOUT;

    my @to;
    foreach ( read_file( "$options->{workdir}/mailto", err_mode => 'silent' ) ) {
        s/^\s*//;
        s/#.*//;
        s/\s*$//;
        next unless $_;
        s/$/\@fi.muni.cz/ unless /\@/;
        push @to, $_;
    }
    push @to, "$ENV{USER}\@fi.muni.cz" unless @to;

    $should_mail = 0 if $options->{'dry-run'};
    my $subj = "[$Subject] Reviewable submissions (" . @new_subs . ' new)';
    say "Subject: $subj" if -t STDOUT;

    for my $to ( @to ) {
        my $msg = MIME::Lite->new(
            From    => $options->{from},
            To      => $to,
            Subject => $subj,
            Data    => encode_utf8( $body )
        );
        $msg->attr( 'content-type' => 'text/plain; charset=utf-8');
        $msg->send( 'sendmail', '/usr/lib/sendmail -t -X ""') if $should_mail;
        say "To: $to" if -t STDOUT;
    }
    say "(No changes made, this is a dry run)" if $options->{'dry-run'};

    return 1;
}
