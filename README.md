Přífraží
========

Rozličné skripty a nástroje pro práci s fragovou databasí.

## Modul `Frag::Misc::SleepyDaemon`

Démon, který většinu času spí, ale má u toho otevřené spojení s databasí.
Při probuzení přes `SIGUSR1` provede `job( $dbhandle, $last_timestamp, $opts )`,
aktualisuje si časové razítko a spí dál.

Bez konfigurační volby `loop` funguje jednorázově.

Ve `workdir` vyrobí skript `wake`, který se dá pouštět třeba z Cronu.

## Oznamovač odevzdání: `submission-announcer.pl`

Ospalý démon, který při probuzení zkontroluje, zda nepřibyla prošedší odevzdání
a zpraví o tom lidi ve `$workdir/mailto` e-mailem.

## Statický přidělovač studentských recensí: `peer-reviews.pl`

Rozháže studentům recense posledního odevzdání zvolené úlohy náhodně tak, aby
každý recensoval N kolegů a sám dostal N recensí. Studenti pak mají použít

```console
$ ib016 review --restore
```

protože z pohledu fragu to vypadá, že ty recense už existují a studenti si je
jen smazali z adresáře.

## Rozesílač výstupu testů pro peer review: `send-test-output-to-reviewers.pl`

Rozešle recensentům výstup testů řešení, která recensují, pokud tyto neprošly.

## Dotaz `without_reviewers.sql`

Zjistí, kdo čeká (a jak dlouho) na recensi, ale nemá přiděleného recensenta.

Příklad použití:
```console
$ psql -h frag-db.fi.muni.cz pv264 < without_reviewers.sql
```
