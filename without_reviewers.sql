with passed as
        (select eo.eval_id from frag.eval_out as eo where eo.group = 'verity' group by eval_id having every(eo.passed)),
     newest as
        (select distinct on (s.author, s.assignment_id) s.author, s.assignment_id as aid, s.id, s.stamp
            from passed join frag.eval as e on passed.eval_id = e.id
            join frag.submission as s on submission_id = s.id
            order by s.author, s.assignment_id, s.stamp desc),
     teachers as
        (select teacher from frag.teacher_list),
     claimed as
        (select author, assignment_id from frag.review join frag.submission as s on s.id = submission_id
            where reviewer in (table teachers))
select convert_from(p.login, 'UTF8') as login, p.name, newest.id as sub_id, a.name as hw, newest.stamp as submitted, now() - newest.stamp as waiting_for
    from newest join frag.person as p on p.id = author
    join frag.assignment as a on a.id = aid
    where (author, aid) not in (table claimed) and author not in (table teachers)
    order by stamp;

